/*** 計算日期，輸入往後推算得天數得到日期
 * @param Days 傳入之往後天數
 * @param days 往後天數
 * @param timeStamp 現在日期
 * @param cur_year 現在年份
 * @param cur_month 現在月份
 * @param cur_day 現在日
 * @return Integer.toString(cur_year)+"/"+Integer.toString(cur_month)+"/"+Integer.toString(cur_day) 將算出日期轉成正確格式字串並回傳
 *  * Example: 輸入:10並且今天是2022/4/2,輸出:2022/4/12
 * Time estimate: O(n)
 */


package test;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class count_date {
	public String  func(String Days) {
		int days = Integer.parseInt(Days);
		String timeStamp = new SimpleDateFormat("yyyy/MM/dd").format(Calendar.getInstance().getTime());
		int cur_year = Integer.parseInt(timeStamp.substring(0,4));
		int cur_month = Integer.parseInt(timeStamp.substring(5,7));
		int cur_day = Integer.parseInt(timeStamp.substring(8,10));
		while(days != 0) {
			if(days >= maxDayInYear(cur_year)) days-= maxDayInYear(cur_year++);
			else if(days >= maxDayInMonth(cur_year,cur_month)) {
				days-=maxDayInMonth(cur_year,cur_month);
				if(cur_month == 12) {
					cur_month = 1;
					cur_year++;
				}
				else cur_month++;
			}
			else {
				if(days+ cur_day <= maxDayInMonth(cur_year,cur_month)) {
					cur_day += days;
					days = 0;
				}
				else {
					days -= (maxDayInMonth(cur_year,cur_month) - cur_day);
					cur_day = 1;
					cur_month ++;
				}
			}
			//System.out.printf("%d/%d/%d\n",cur_year,cur_month,cur_day);
		}
		System.out.printf("往後%s天是%d/%d/%d\n",Days,cur_year,cur_month,cur_day);
		return Integer.toString(cur_year)+"/"+Integer.toString(cur_month)+"/"+Integer.toString(cur_day);
	}
	public static int maxDayInMonth(int year, int month) {
	    int max = 30;
	    if (month == 1 | month == 3 | month == 5 | month == 7 | month == 8 | month == 10 | month == 12) max = 31;
	    else if (month == 2) max = 28;
	    else if (month == 2 & (year % 4 == 0) && (year % 100 != 0) || (year % 400 == 0)) max = 29;
	    return max;
	}
	public static int maxDayInYear(int year) {
		return ((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0) ?  366:365;
	}
}
