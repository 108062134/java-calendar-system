/*** 計算天數，輸入欲查詢日期(年/月/日)得到距離天數
 * @param day 輸入之目標日期
 * @param target_year 輸入之目標年份
 * @param target_month 輸入之目標月份
 * @param target_day 輸入之目標日
 * @param timeStamp 現在日期
 * @param cur_year 現在年份
 * @param cur_month 現在月份
 * @param cur_day 現在日
 * @param cnt 得到之距離天數
 * @return Integer.toString(cnt) 將算出天數轉換為字串並回傳
 *  * Example: 輸入:2022/4/12並且今天是2022/4/2,輸出:10天
 * Time estimate: O(n)
 */


package test;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class count_day {
	public String func(String day) {
		int target_year,target_mon,target_day;
		target_year = Integer.parseInt(day.substring(0,4));
		target_mon = Integer.parseInt(day.substring(5,7));
		target_day = Integer.parseInt(day.substring(8,10));
		String timeStamp = new SimpleDateFormat("yyyy/MM/dd").format(Calendar.getInstance().getTime());
		int cur_year = Integer.parseInt(timeStamp.substring(0,4));
		int cur_month = Integer.parseInt(timeStamp.substring(5,7));
		int cur_day = Integer.parseInt(timeStamp.substring(8,10));
		int cnt = 0;
		//System.out.printf("%d %d %d %d %d %d\n",cur_year,cur_month,cur_day,target_year,target_mon,target_day);
		
		for(int i=cur_year;i<=target_year;i++) {
			if(i == target_year) {
				for(int j=cur_month+1;j<=target_mon-1;j++) {
					cnt += maxDayInMonth(cur_year,j);
				}
				if(cur_month == target_mon)	cnt += target_day - cur_day;
				else cnt += (maxDayInMonth(cur_year,cur_month)-cur_day)+ target_day;
			}else if(target_mon > cur_month || ( target_mon== cur_month && target_day >= cur_day)){
				cnt += ((i % 4 == 0) && (i % 100 != 0)) || (i % 400 == 0) ?  366:365;
			}else {
				for(int j=cur_month+1;j<=12;j++) {
					cnt += maxDayInMonth(cur_year,j);
				}
				cnt += 31 - cur_day;
				cur_day = 1;
				cur_year++;
				cur_month = 1;
			}
			//System.out.printf("%d\n",cnt);
		}
		System.out.println(day+"距離今天還有"+cnt+"天\n");
		return Integer.toString(cnt);
	}
	public static int maxDayInMonth(int year, int month) {
	    int max = 30;
	    if (month == 1 | month == 3 | month == 5 | month == 7 | month == 8 | month == 10 | month == 12) max = 31;
	    else if (month == 2) max = 28;
	    else if (month == 2 & (year % 4 == 0) && (year % 100 != 0) || (year % 400 == 0)) max = 29;
	    return max;
	}
}

