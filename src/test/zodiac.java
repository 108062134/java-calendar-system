/*** 將西元紀年轉為干支紀年與生肖
 * @param year 西元紀年，輸入
 * @param skyBranch 10天干
 * @param earthBranch 12地支
 * @param animalBranch 12生肖
 * @param tmp 暫存輸出字串
 * @return ans 輸出字串
 *  * Example: 輸入:2022,輸出:2022是壬寅年，屬虎
 * Time estimate: O(1)
 */

package test;

public class zodiac {
	public static final char[] skyBranch = new char[] {'甲','乙','丙','丁','戊','己','庚','辛','壬','癸'};

	public static final char[] earthBranch = new char[] {'子','丑','寅','卯','辰','巳','午','未','申','酉','戌','亥'};
	
	public static final char[] animalBranch = new char[] {'鼠','牛','虎','兔','龍','蛇','馬','羊','猴','雞','狗','豬'};
	
	public String func (String args) {
		
//		String args;
//		Scanner scanner = new Scanner(System.in);
//		
//		System.out.println("請輸入數字：");
//        args = scanner.nextLine();
		String ans = new String("");
        int year = Integer.valueOf(args).intValue();
		try {
			ans = calculate(year);
//			System.out.print(ans);
		} catch (RuntimeException e) {
			System.out.println("Year" + year + " met exception.");
		}
		return ans;
	}
	
	private static String calculate(int i) {
		if (i < 4) {
			throw new IllegalArgumentException("The starting year must be greater than 4");
		}
		int realYear = i - 4;
		String tmp =  (i + "是" + skyBranch[realYear % 10] + earthBranch[realYear % 12] + "年，屬" + animalBranch[realYear % 12] + "\n");
		System.out.print(tmp);
		return tmp;
	}
}