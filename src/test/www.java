/*** 計算天數，輸入欲查詢日期(年/月/日)得到距離天數
 * @param A calendar function
 * @param B zodiac function
 * @param C count_day function
 * @param D count_date function
 * @param E ErrorDetect function
 * @param k 輸入，要執行的method
 * @param tmp 暫存要執行的method
 * @param ans 確認格式，一為正確
 *  * Example: 輸入:C與2022/4/12並且今天是2022/4/2,輸出:2022/04/12距離今天還有10天
 * Time estimate: O(n)
 */

package test;
import java.util.Scanner;

public class www {
	public static void main(String[] arg)
	{	
		while(true) {
			calendar A = new calendar();
			zodiac B = new zodiac();
			count_day C = new count_day();
			count_date D = new count_date();
			ErrorDetect E = new ErrorDetect();
			System.out.println("請輸入指令或E(結束使用)?\n");   // 	E or Q	?
			System.out.println("輸入指令");
			System.out.println("1) A 顯示該月月曆");
			System.out.println("2) B 西元轉換干支、生肖");
			System.out.println("3) C 計算天數");
			System.out.println("4) D 計算日期");
			System.out.println("5) E 離開");
			Scanner keyin = new Scanner(System.in);
			String k = keyin.next();
			char tmp = k.charAt(0);
			if(	tmp >= 'A' && tmp <= 'E' && k.length() == 1 ) {
				if(tmp == 'E'){
					System.out.println("結束了!");
					System.exit(0);
				}else {
					if(tmp == 'B') System.out.println("請輸入欲查詢年:");	
					else if(tmp == 'C' || tmp == 'A') System.out.println("請輸入欲查詢日期(年/月/日):");
					else if(tmp == 'D') System.out.println("請輸入往後推算的天數:");
					String s = keyin.next();
					int ans = E.func(tmp, s);
					if(ans == 1) {
						if(tmp == 'B') B.func(s);	
						else if(tmp == 'C') C.func(s);
						else if(tmp == 'D') D.func(s);
						else if(tmp == 'A') A.func(s);
					}else System.out.printf("輸入格式錯誤\n");
				}
			}else{
				System.out.println("指令錯了!");
			}
			
		}
	}
}