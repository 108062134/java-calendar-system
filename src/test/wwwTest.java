package test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class wwwTest {
	count_date  test1 = null;
	count_day test2 = null;
	ErrorDetect test3 = null;
	zodiac test4 = null;
	calendar test5 = null;
	@BeforeEach
	void setUp() throws Exception {
		test1 = new count_date();
		test2 = new count_day();
		test3 = new ErrorDetect();
		test4 = new zodiac();
		test5 = new calendar();
	}

	@AfterEach
	void tearDown() throws Exception {
		test1 = null;
		test2 = null;
		test3 = null;
		test4 = null;
		test5 = null;
	}
	

	@Test
	void test() {
		/**
		 * test method:count_date()
		 * current date:2022/4/2
		 * test date:2022/4/22,2023/4/2,2023/4/17,2023/1/17,2024/4/2,2024/4/12,2025/4/2,2025/4/6,2025/5/8
		 */
		assertEquals("2022/4/22",test1.func("20"));
		assertEquals("2023/4/2",test1.func("365"));
		assertEquals("2023/4/17",test1.func("380"));
		assertEquals("2023/1/17",test1.func("290"));
		assertEquals("2024/4/2",test1.func("730"));
		assertEquals("2024/4/12",test1.func("740"));
		assertEquals("2025/4/2",test1.func("1096")); 
		assertEquals("2025/4/6",test1.func("1100"));
		assertEquals("2025/5/8",test1.func("1132"));
		/**
		 * test method:count_day()
		 * current date:2022/4/2
		 * test day:30,5,290,365,380,730,740,1096,1100,1132
		 */
		assertEquals("30",test2.func("2022/05/02"));
		assertEquals("5",test2.func("2022/04/07"));
		assertEquals("290",test2.func("2023/01/17"));
		assertEquals("365",test2.func("2023/04/02"));
		assertEquals("380",test2.func("2023/04/17"));
		assertEquals("730",test2.func("2024/04/02"));
		assertEquals("740",test2.func("2024/04/12"));
		assertEquals("1096",test2.func("2025/04/02"));
		assertEquals("1100",test2.func("2025/04/06"));
		assertEquals("1132",test2.func("2025/05/08"));
		
		/**
		 * test method:count_date()
		 * current date:2022/4/2
		 * test date:B 20w,B 20.1,B -3,B 100,B 1000,D 20w,D 20.1,D -3,D 100,D 1000
		 */	
		assertEquals(0,test3.func('B', "20w"));
		assertEquals(0,test3.func('B', "20.1"));
		assertEquals(0,test3.func('B', "-3"));
		assertEquals(1,test3.func('B', "100"));
		assertEquals(1,test3.func('B', "1000"));
		
		assertEquals(0,test3.func('D', "20w"));
		assertEquals(0,test3.func('D', "20.1"));
		assertEquals(0,test3.func('D', "-3"));
		assertEquals(1,test3.func('D', "100"));
		assertEquals(1,test3.func('D', "1000"));
		

		/**
		 * test method:zadiac()
		 * current date:2022/4/2
		 * test date:2022,1950,1980,2003
		 */	
		assertEquals("2022是壬寅年，屬虎",test4.func("2022"));
		assertEquals("1950是庚寅年，屬虎",test4.func("1950"));
		assertEquals("1980是庚申年，屬猴",test4.func("1980"));
		assertEquals("2003是癸未年，屬羊",test4.func("2003"));
		

		/**
		 * test method:calendar()
		 * current date:2022/4/2
		 * test date:1576/03/25,1432/11/23
		 */	
		assertEquals(
				"Sun   Mon   Tue   Wed   Thu   Fri   Sat\r\n"
				+ "                         1     2     3    \r\n"
				+ " 4     5     6     7     8     9    10    \r\n"
				+ "11    12    13    14    15    16    17    \r\n"
				+ "18    19    20    21    22    23    24    \r\n"
				+ "25    26    27    28    29    30    31    ",test5.func("1576/03/25"));

		assertEquals(
				"Sun   Mon   Tue   Wed   Thu   Fri   Sat\r\n"+
				"                                     1    \r\n"+
				" 2     3     4     5     6     7     8    \r\n"+    
				" 9    10    11    12    13    14    15    \r\n"+
				"16    17    18    19    20    21    22    \r\n"+
				"23    24    25    26    27    28    29    \r\n"+
				"30    ",test5.func("1432/11/23"));
		
	}
	
	@Test
	/**
	 * test method:count_date()+count_day()+ErrordDetect()+zadiac()+calendar()
	 * current date:2022/4/2
	 * test data:20,2024/04/12,B,1000,1432/11/23
	 */	
	void integration_test1() {
		String expected = "2022/4/22" + "740" + "1" + "1943是癸未年，屬羊" + '\n' + "Sun   Mon   Tue   Wed   Thu   Fri   Sat\r\n"+
				"                                     1    \r\n"+
				" 2     3     4     5     6     7     8    \r\n"+    
				" 9    10    11    12    13    14    15    \r\n"+
				"16    17    18    19    20    21    22    \r\n"+
				"23    24    25    26    27    28    29    \r\n"+
				"30    "; 
		String a = test1.func("20");
		String b = test2.func("2024/04/12");
		int c = test3.func('B', "1000");
		String d = test4.func("1943");
		String e = test5.func("1432/11/23");
		String tmpc = Integer.toString(c);
		assertEquals(expected,a+b+tmpc+d+e);
	}
	
	@Test
	/**
	 * test method:count_date()+count_day()+ErrordDetect()+zadiac()+calendar()
	 * current date:2022/4/2
	 * test data:730,2025/05/08,D,-3,1576/03/25
	 */	
	void integration_test2() {
		String expected = "2024/4/2" + "1132" + "0" + "1890是庚寅年，屬虎" + '\n' + "Sun   Mon   Tue   Wed   Thu   Fri   Sat\r\n"
				+ "                         1     2     3    \r\n"
				+ " 4     5     6     7     8     9    10    \r\n"
				+ "11    12    13    14    15    16    17    \r\n"
				+ "18    19    20    21    22    23    24    \r\n"
				+ "25    26    27    28    29    30    31    "; 
		String a = test1.func("730");
		String b = test2.func("2025/05/08");
		int c = test3.func('D', "-3");
		String d = test4.func("1890");
		String e = test5.func("1576/03/25");
		String tmpc = Integer.toString(c);
		assertEquals(expected,a+b+tmpc+d+e);
	}

}
