/*** 輸入日期顯示該月月曆
 * @param C 日曆物件
 * @param day 輸入之目標日期
 * @param target_year 輸入之目標年份
 * @param target_month 輸入之目標月份
 * @param target_day 輸入之目標日
 * @param count 暫存日期以對應星期幾
 * @param maxDay 目標月份之天數
 * @param max 月份最大天數
 *  * Example: 輸入:1305/09/13,輸出:
Sun   Mon   Tue   Wed   Thu   Fri   Sat
                   1     2     3     4    
 5     6     7     8     9    10    11    
12    13    14    15    16    17    18    
19    20    21    22    23    24    25    
26    27    28    29    30   
 * Time estimate: O(n)
 */


package test;
import java.util.Calendar;
import java.text.SimpleDateFormat;

public class calendar {
	public String func(String day) {
		Calendar C = Calendar.getInstance();
		String ans = new String("");
		int target_year,target_mon,target_day;
		target_year = Integer.parseInt(day.substring(0,4));
		target_mon = Integer.parseInt(day.substring(5,7));
		target_day = Integer.parseInt(day.substring(8,10));
		C.set(target_year, target_mon - 1, 1);
		int start = C.get(C.DAY_OF_WEEK);
        int count = start - 1;
        int maxDay = maxDayInMonth(target_year, target_mon);
        System.out.println("Sun   Mon   Tue   Wed   Thu   Fri   Sat");
        ans = ans +  "Sun   Mon   Tue   Wed   Thu   Fri   Sat"+'\n';
        for (int i = 1; i < start; i++) {
            System.out.printf("%6s", " ");
            ans = ans + "      ";
        }
        for (int i = 1; i <= maxDay; i++) {
            System.out.printf("%2d%s", i, "    "); 
            ans = ans + ((i<10)?(" "+i):i) + "    ";
            count++;
            if (count >= 7) { 
                count = 0;
                System.out.print('\n');
                ans = ans +  "\n";
            }
        }
        return ans;
	}
	public static int maxDayInMonth(int year, int month) {
        int max = 30;
        if (month == 1 | month == 3 | month == 5 | month == 7 | month == 8 | month == 10 | month == 12) max = 31;
        else if (month == 2) max = 28;
        else if (month == 2 & (year % 4 == 0) && (year % 100 != 0) || (year % 400 == 0)) max = 29;
        return max;
    }
}
