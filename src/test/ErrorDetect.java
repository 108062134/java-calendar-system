/*** r檢查是否輸入錯誤
 * @param type 輸入，何種類別
 * @param s 輸入，輸入之字串(日期或天數)
 * @param tmp 暫存天數
 * @param timeStamp 現在日期
 * @param cur_year 現在年份
 * @param cur_month 現在月份
 * @param cur_day 現在日
 * @param year 輸入之年份
 * @param month 輸入之月份
 * @param day 輸入之日
 * @param max 月份天數
 * @return ans 回傳是否正確，正確為一，錯誤為零
 *  * Example: 輸入:D與2200/11/30,輸出:0
 * Time estimate: O(1)
 */

package test;

import java.text.SimpleDateFormat;
import java.util.Calendar;

//import org.apache.commons.lang3.StringUtils;
public class ErrorDetect {
	public int func(char type,String s) {
		int ans = 1;
		int tmp = 0;
		if(type == 'B' || type == 'D') {
			try {
				tmp = Integer.parseInt(s);
			}catch(Exception e) {
				ans = 0;
			}
			if(tmp <= 0) ans =0;
		}else if(type == 'C') {
			try {
				int year,mon,day;
				year = Integer.parseInt(s.substring(0,4));
				mon = Integer.parseInt(s.substring(5,7));
				day = Integer.parseInt(s.substring(8,10));
				String timeStamp = new SimpleDateFormat("yyyy/MM/dd").format(Calendar.getInstance().getTime());
				int cur_year = Integer.parseInt(timeStamp.substring(0,4));
				int cur_month = Integer.parseInt(timeStamp.substring(5,7));
				int cur_day = Integer.parseInt(timeStamp.substring(8,10));
				if(year <= 2021 || (mon<1||mon>12) || maxDayInMonth(year, mon)<day) ans = 0 ;
				if(year < cur_year || (year == cur_year&& mon<cur_month) || (year == cur_year && mon == cur_month && day < cur_day)) ans = 0;
			}catch(Exception e) {
				ans = 0;
			}
			
		} else if(type == 'A') {
			try {
				int year,mon,day;
				year = Integer.parseInt(s.substring(0,4));
				mon = Integer.parseInt(s.substring(5,7));
				day = Integer.parseInt(s.substring(8,10));
				if(year <= 0 || (mon<1||mon>12) || maxDayInMonth(year, mon)<day) ans = 0 ;
			}catch(Exception e) {
				ans = 0;
		}
		
	}
		return ans;
	}
	public static int maxDayInMonth(int year, int month) {
	    int max = 30;
	    if (month == 1 | month == 3 | month == 5 | month == 7 | month == 8 | month == 10 | month == 12) max = 31;
	    else if (month == 2) max = 28;
	    else if (month == 2 & (year % 4 == 0) && (year % 100 != 0) || (year % 400 == 0)) max = 29;
	    return max;
	}
}
